# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from virtual_school import settings

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^student/', include('student.urls', namespace='student')),
    url(r'^teacher/', include('teacher.urls', namespace='teacher')),
    url(r'^accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^$', 'accounts.views.login')
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()

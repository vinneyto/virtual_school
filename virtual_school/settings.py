# -*- coding: utf-8 -*-
"""
Django settings for virtual_school project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['VIRTUAL_SCHOOL_KEY']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

LOGIN_URL = '/accounts/login'

AUTH_USER_MODEL = 'customuser.CustomUser'

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'tinymce',
    'customuser',
    'school',
    'student',
    'teacher',
    'accounts',
    'south'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'virtual_school.urls'

WSGI_APPLICATION = 'virtual_school.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
    os.path.join(BASE_DIR, 'accounts', 'templates'),
    os.path.join(BASE_DIR, 'student', 'templates')
)


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

TINYMCE_DEFAULT_CONFIG = {'theme': "advanced", 'theme_advanced_toolbar_location': "top", 'height': '400',
                          'forced_root_block': ''}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

MEDIA_ROOT = os.path.join(BASE_DIR, 'files', 'media')
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(BASE_DIR, 'files', 'static')
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

ADMIN_MEDIA_PREFIX = '/static/admin/'


# fabric settings

FABRIC = {
    "SSH_USER": "vinneyto", # SSH username for host deploying to
    "HOSTS": ALLOWED_HOSTS[:1], # List of hosts to deploy to (eg, first host)
    "DOMAINS": ALLOWED_HOSTS, # Domains for public site
    "REPO_URL": "https://vinneyto@bitbucket.org/vinneyto/virtual_school.git", # Project's repo URL
    "VIRTUALENV_HOME":  "/webapps", # Absolute remote path for virtualenvs
    "PROJECT_NAME": "virtual_school", # Unique identifier for project
    "REQUIREMENTS_PATH": None, # Project's pip requirements
    "GUNICORN_PORT": 1111, # Port gunicorn will listen on
    "LOCALE": "ru_RU.UTF-8", # Should end with ".UTF-8"
    "DB_PASS": "qazxcv", # Live database password
    "ADMIN_PASS": "q1w2e3r4", # Live admin user password
    "SECRET_KEY": SECRET_KEY
}
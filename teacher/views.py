# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from school.models import Teacher


def teacher_required(function):
    def wrapper(request, *args, **kw):
        cast = request.user.cast()
        if type(cast) is Teacher:
            request.teacher = cast
            return function(request, *args, **kw)
        else:
            return redirect('accounts:logout')
    return wrapper


@login_required
@teacher_required
def index(request):
    return render(request, 'base_site.html', {
        'content_text': u'Личный кабинет учителя, число классов: %d' % request.teacher.classrooms.count()
    })
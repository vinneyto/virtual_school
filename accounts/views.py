# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.core.context_processors import csrf
from django.contrib import auth

from customuser.models import CustomUser
from school.models import Teacher, Student


def login(request):
    args = {}
    args.update(csrf(request))
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:

            cast = user.cast()

            if type(cast) is Student:
                auth.login(request, user)
                return redirect('student:index')

            elif type(cast) is Teacher:
                auth.login(request, user)
                return redirect('teacher:index')

            else:
                args['has_error'] = True
                args['error'] = u'Данная форма авторизации предназначена только для учеников и преподавателей'
                return render(request, 'accounts/login.html', args)
        else:
            args['has_error'] = True
            args['error'] = u'Не вернео имя пользователя или пароль'
            return render(request, 'accounts/login.html', args)
    else:
        return render(request, 'accounts/login.html', args)


def logout(request):
    auth.logout(request)
    return redirect('/')
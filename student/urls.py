# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'student.views.index', name='index'),
    url(r'^$', 'student.views.tasks', name='tasks'),
    url(r'^$', 'student.views.docs', name='docs'),
)
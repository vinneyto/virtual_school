# -*- coding: utf-8 -*-
from django.utils import timezone
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from school.models import Student


def student_required(function):
    def wrapper(request, *args, **kw):
        cast = request.user.cast()
        if type(cast) is Student:
            request.student = cast
            return function(request, *args, **kw)
        else:
            return redirect('accounts:logout')
    return wrapper


@login_required
@student_required
def index(request):
    args = {
        'request': request,
        'student': request.student,
        'tasks': request.student.task_set.filter(release_date__gte=timezone.now())
    }
    return render(request, 'student/index.html', args)


@login_required
@student_required
def tasks(request):
    pass


@login_required
@student_required
def docs(request):
    pass

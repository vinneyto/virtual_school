# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import AbstractUser


class InheritanceAbstractUser(AbstractUser):
    """
    An abstract base class that provides a ``real_type`` FK to ContentType.

    For use in trees of inherited models, to be able to downcast
    parent instances to their child types.

    """
    real_type = models.ForeignKey(ContentType, editable=False)

    def save(self, *args, **kwargs):
        if not self.id:
            self.real_type = self._get_real_type()
        super(InheritanceAbstractUser, self).save(*args, **kwargs)

    def _get_real_type(self):
        return ContentType.objects.get_for_model(type(self))

    def cast(self):
        return self.real_type.get_object_for_this_type(pk=self.pk)

    class Meta:
        abstract = True


class CustomUser(InheritanceAbstractUser):
    """
    Модель пользователя унаследованная от дефолтного.
    Добавлено поле с отчеством
    """
    middle_name = models.CharField(max_length=200, verbose_name=u'Отчество', blank=True)

    def get_full_name(self):
        full_name = '%s %s %s' % (self.first_name, self.last_name, self.middle_name, )
        return full_name.strip()


    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'

# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from school.forms import TeacherCreationForm, StudentCreationForm

from school.models import (
    Teacher,
    Student,
    Classroom,
    Subject,
    VirtualLab,
    LabVariant,
    Task
)

#Регистрация преподавателя
class TeacherAdmin(UserAdmin):
    list_display = ('username', 'first_name', 'last_name', 'middle_name')
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2', 'first_name', 'last_name', 'middle_name')}
        ),
    )
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'middle_name', 'classrooms')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_form = TeacherCreationForm
    filter_horizontal = ('groups', 'user_permissions', 'classrooms', )


admin.site.register(Teacher, TeacherAdmin)

#регистрация учащегося
class StudentAdmin(UserAdmin):
    list_display = ('username', 'first_name', 'last_name', 'middle_name', 'classroom')
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2', 'first_name', 'last_name', 'middle_name', 'classroom')}
        ),
    )
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'middle_name', 'classroom')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_form = StudentCreationForm


admin.site.register(Student, StudentAdmin)


#регистрация класса
class StudentsInline(admin.StackedInline):
    model = Student
    fields = ('username', 'first_name', 'last_name', 'middle_name', 'classroom')
    extra = 0


class ClassroomAdmin(admin.ModelAdmin):
    inlines = [StudentsInline]

    class Media:
        js = (
            'school/js/collapsed-stacked-inline.js',
        )


admin.site.register(Classroom, ClassroomAdmin)


#регистрация предметов
class VirtualLabInline(admin.StackedInline):
    model = VirtualLab
    extra = 0

    class Media:
        js = (
            'school/js/collapsed-stacked-inline.js',
        )


class SubjectAdmin(admin.ModelAdmin):
    inlines = [VirtualLabInline]


admin.site.register(Subject, SubjectAdmin)


#регистрация лабораторных работ
class LabVariantInline(admin.StackedInline):
    model = LabVariant
    extra = 0


class VirtualLabAdmin(admin.ModelAdmin):
    inlines = [LabVariantInline]


admin.site.register(VirtualLab, VirtualLabAdmin)

#рагистрация вариантов
admin.site.register(LabVariant)

#рагистрация заданий
admin.site.register(Task)

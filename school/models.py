# -*- coding: utf-8 -*-
from django.db import models
from customuser.models import CustomUser
from tinymce.models import HTMLField


class Classroom(models.Model):
    """
    Модель класса
    """

    NUMBER_CHOICES = (
        (6, '6'),
        (7, '7'),
        (8, '8'),
        (9, '9'),
        (10, '10'),
        (11, '11'),
    )

    number = models.IntegerField(default=7, choices=NUMBER_CHOICES, verbose_name=u'цифра')

    LETTER_CHOICES = (
        (u'А', u'А'),
        (u'Б', u'Б'),
        (u'В', u'В'),
        (u'Г', u'Г'),
        (u'Д', u'Д'),
    )

    letter = models.CharField(max_length=1, choices=LETTER_CHOICES, verbose_name=u'буква', default='А')

    def __unicode__(self):
        return '%d%s' % (self.number, self.letter)

    class Meta:
        verbose_name = u'Класс'
        verbose_name_plural = u'Классы'


class Teacher(CustomUser):
    """
    Модель учителя
    """

    classrooms = models.ManyToManyField(Classroom, verbose_name=u'Классы', blank=True)

    def __unicode__(self):
        return self.get_full_name()

    class Meta:
        verbose_name = u'Преподаватель'
        verbose_name_plural = u'Преподаватели'


class Student(CustomUser):
    """
    Модель ученика
    """

    classroom = models.ForeignKey(Classroom, verbose_name=u'Класс')

    def __unicode__(self):
        return '%s - %s' % (self.get_full_name(), self.classroom.__unicode__())

    class Meta:
        verbose_name = u'Учащийся'
        verbose_name_plural = u'Учащиеся'


class Subject(models.Model):
    """
    Модель предмета для лабораторной работы
    """

    title = models.CharField(max_length=200, verbose_name=u'Название')
    desc = HTMLField(verbose_name=u'Описание', blank=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Предмет'
        verbose_name_plural = u'Предметы'


class VirtualLab(models.Model):
    """
    Класс виртуальной лабораторной работы
    """

    title = models.CharField(max_length=200, verbose_name=u'Название')
    desc = HTMLField(verbose_name=u'Описание', blank=True)
    subject = models.ForeignKey(Subject)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Лабораторная работа'
        verbose_name_plural = u'Лабораторные работы'


class LabVariant(models.Model):
    """
    Класс варианта лабораторной работы
    """

    title = models.CharField(max_length=200, verbose_name=u'Название')
    desc = HTMLField(verbose_name=u'Описание', blank=True)
    file = models.FileField(upload_to='labs')

    lab = models.ForeignKey(VirtualLab, verbose_name=u'Лабораторная работа')

    is_demo = models.BooleanField(default=False, verbose_name=u'Демоверсия')

    def __unicode__(self):
        return '%s - %s' % (self.lab.title, self.title)

    class Meta:
        verbose_name = u'Вариант'
        verbose_name_plural = u'Варианты лабораторных работ'


class Task(models.Model):
    """
    Задание для учащегося
    """

    student = models.ForeignKey(Student, verbose_name=u'Студент')
    teacher = models.ForeignKey(Teacher, verbose_name=u'Преподаватель')
    lab = models.ForeignKey(VirtualLab, verbose_name=u'Лабораторная работа')

    text = HTMLField(verbose_name=u'Задание', blank=True)
    release_date = models.DateField(verbose_name=u'Дата сдачи')

    STATE_CHOICES = (
        (0, u'допуск'),
        (1, u'выполнение'),
        (2, u'защита'),
        (3, u'сдана')
    )

    state = models.IntegerField(default=0, choices=STATE_CHOICES,
                                verbose_name=u'Стадия выполнения', blank=True)

    MARK_CHOICES = (
        (0, '0'),
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    )

    mark = models.IntegerField(default=0, choices=MARK_CHOICES,
                               verbose_name=u'оценка', blank=True)

    def __unicode__(self):
        return '%s - %s' % (self.teacher.get_full_name(), self.lab.title)

    class Meta:
        verbose_name = u'Задание'
        verbose_name_plural = u'Задания'



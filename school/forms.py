# -*- coding: utf-8 -*-
from school.models import Teacher, Student
from customuser.forms import CustomUserCreationForm


class TeacherCreationForm(CustomUserCreationForm):
    class Meta:
        model = Teacher
        fields = ('username', 'first_name', 'last_name', 'middle_name')


class StudentCreationForm(CustomUserCreationForm):
    class Meta:
        model = Student
        fields = ('username', 'first_name', 'last_name', 'middle_name', 'classroom')